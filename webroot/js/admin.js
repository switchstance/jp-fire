$(function () {
    if ($('[data-toggle="tooltip"]').length > 0) {
        $('[data-toggle="tooltip"]').tooltip();
    }

    $('.deleteQuadrant').on('click', function(e) {
        e.preventDefault();
        var quadrant_id = $(this).data('id');
        var quadrant_name = $(this).parents('.media-body').eq(0).find('.files-title').eq(0).text();
        if(typeof quadrant_id == 'undefined' || typeof quadrant_name == 'undefined') {
            showError('Error occurred while trying to delete this quadrant. Please contact Support Team.');
        }

        $('#delete-survey-quadrant-id').val(quadrant_id);
        $('#confirm-quadrant-name').text(quadrant_name);
        $('#modal-delete-survey-quadrant').modal('show');
    });

    $('.deleteSection').on('click', function(e) {
        e.preventDefault();
        var section_id = $(this).data('id');
        var section_number = $(this).parents('.widget-header').eq(0).find('.section-number').eq(0).text();
        if(typeof section_id == 'undefined' || typeof section_number == 'undefined') {
            showError('Error occurred while trying to delete this section. Please contact Support Team.');
        }

        $('#delete-survey-section-id').val(section_id);
        $('#confirm-section-number').text(section_number);
        $('#modal-delete-survey-section').modal('show');
    });

    $('.deleteQuestion').on('click', function(e) {
        e.preventDefault();
        var question_id = $(this).data('id');
        var question_number = $(this).parents('.question-row').eq(0).find('.question-number').eq(0).text();
        if(typeof question_id == 'undefined' || typeof question_number == 'undefined') {
            showError('Error occurred while trying to delete this question. Please contact Support Team.');
        }

        $('#delete-survey-question-id').val(question_id);
        $('#confirm-question-number').text(question_number);
        $('#modal-delete-survey-question').modal('show');
    });

    $('.editSection').on('click', function(e) {
        e.preventDefault();
        var section_id = $(this).data('id');

        if(typeof section_id == 'undefined') {
            showError('Error occurred while trying to edit this section. Please contact Support Team.');
        }

        $('#loader-modal').modal('show');

        $.ajax({
            method: 'GET',
            url: '/admin/surveys/edit-survey-section/'+section_id+'.json',
            success: function(resp) {
                $('#modal-add-survey-section').find('.modal-title').eq(0).text('Edit Section');
                $('#modal-add-survey-section').find('form').eq(0).attr('action', '/admin/surveys/edit-survey-section/'+section_id);
                $('#section-name').val(resp.data.section_name);
                $('#modal-add-survey-section').modal('show');
            },
            error: function() {
                showError('Error occurred while trying to edit this section. Please contact Support Team.');
            }
        }).always(function () {
            $('#loader-modal').modal('hide');
        });
    });

    $('#modal-add-survey-section').on('hidden.bs.modal', function () {
        $(this).find('.modal-title').eq(0).text('Add Section');
        $('#section-name').val('');
        $(this).find('form').eq(0).attr('action', '/admin/surveys/add-survey-section');
    });

    $('.editQuestion').on('click', function(e) {
        e.preventDefault();
        var question_id = $(this).data('id');

        if(typeof question_id == 'undefined') {
            showError('Error occurred while trying to edit this question. Please contact Support Team.');
        }

        var modal = $(this).parents('.section-wrapper').eq(0).find('.modal-add-question').eq(0);
        $('#loader-modal').modal('show');

        $.ajax({
            method: 'GET',
            url: '/admin/surveys/edit-survey-question/'+question_id+'.json',
            success: function(resp) {
                modal.find('.modal-subtitle').eq(0).text('Edit Question');
                modal.find('form').eq(0).attr('action', '/admin/surveys/edit-survey-question/'+question_id);
                modal.find('.survey_question_content').eq(0).val(resp.data.survey_question_content);
                if(resp.data.answer_type_id) {
                    modal.find('.answer-type-id').eq(0).val(resp.data.answer_type_id);
                } else {
                    modal.find('.answer-type-id').eq(0).val('');
                }
                if(resp.data.answer_type_template_id) {
                    modal.find('.answer-type-template-id').eq(0).val(resp.data.answer_type_template_id);
                } else {
                    modal.find('.answer-type-template-id').eq(0).val('');
                }
                if(resp.data.is_required === true) {
                    modal.find('.is-required').eq(0).prop('checked', true);
                } else {
                    modal.find('.is-required').eq(0).prop('checked', false);
                }
                modal.modal('show');
            },
            error: function() {
                showError('Error occurred while trying to edit this question. Please contact Support Team.');
            }
        }).always(function () {
            $('#loader-modal').modal('hide');
        });
    });

    $('.modal-add-question').on('hidden.bs.modal', function () {
        $(this).find('.modal-subtitle').eq(0).text('Add New Question');
        $(this).find('.survey_question_content').eq(0).val('');
        $(this).find('.answer-type-id').eq(0).val('');
        $(this).find('.answer-type-template-id').eq(0).val('');
        $(this).find('form').eq(0).attr('action', '/admin/surveys/add-survey-question');
    });

    $('.editQuadrant').on('click', function(e) {
        e.preventDefault();
        var quadrant_id = $(this).data('id');

        if(typeof quadrant_id == 'undefined') {
            showError('Error occurred while trying to edit this quadrant. Please contact Support Team.');
        }

        $('#loader-modal').modal('show');

        $.ajax({
            method: 'GET',
            url: '/admin/surveys/edit-survey-quadrant/'+quadrant_id+'.json',
            success: function(resp) {
                $('#modal-add-survey-quadrant').find('.modal-title').eq(0).text('Edit Quadrant');
                $('#modal-add-survey-quadrant').find('form').eq(0).attr('action', '/admin/surveys/edit-survey-quadrant/'+quadrant_id);
                $('#quadrant-name').val(resp.data.quadrant_name);
                $('#info-content').val(resp.data.info_content);
                $('#modal-add-survey-quadrant').modal('show');
            },
            error: function() {
                showError('Error occurred while trying to edit this quadrant. Please contact Support Team.');
            }
        }).always(function () {
            $('#loader-modal').modal('hide');
        });
    });

    $('#modal-add-survey-quadrant').on('hidden.bs.modal', function () {
        $(this).find('.modal-title').eq(0).text('Add Quadrant');
        $('#quadrant-name').val('');
        $(this).find('form').eq(0).attr('action', '/admin/surveys/add-survey-quadrant');
    });

    $('.toggle-quadrants-section').on('click', function() {
        var quadrantsRow = $('.quadrants-row');
        if (quadrantsRow.hasClass('d-none')) {
            quadrantsRow.removeClass('d-none');
        } else {
            quadrantsRow.addClass('d-none');
        }
    });
});

function showError(error_text) {
    new Noty({type:"error",layout:"topRight",text:error_text, progressBar:true, timeout:2500, animation:{open:"animated bounceInRight", close:"animated bounceOutRight"}}).show();
}